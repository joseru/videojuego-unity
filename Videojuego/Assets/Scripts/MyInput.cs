﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MyInput : MonoBehaviour
{
    public string horizontalAxis = "Horizontal";
    public string verticalAxis = "Vertical";
    public string horizontalCameraAxis = "Mouse X";
    public string verticalCameraAxis = "Mouse Y";
    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode shootKey = KeyCode.Mouse0;
    public KeyCode reloadKey = KeyCode.R;
    public KeyCode actionKey = KeyCode.F;

    public float Horizontal() {
        return Input.GetAxis(horizontalAxis);
    }

    public float Vertical() {
        return Input.GetAxis(verticalAxis);
    }

    public float HorizontalCamera() {
        return Input.GetAxis(horizontalCameraAxis);
    }

    public float VerticalCamera() {
        return Input.GetAxis(verticalCameraAxis);
    }

    public bool Jump() {
        return Input.GetKey(jumpKey);
    }

    public bool Shoot() {
        return Input.GetKeyDown(shootKey);
    }

    public bool Reload() {
        return Input.GetKeyDown(reloadKey);
    }

    public bool Action() {
        return Input.GetKeyDown(actionKey);
    }

}
