﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskCollision : MonoBehaviour
{
    public string headTag = "head";
    void OnCollisionEnter(Collision collision)
    {
        // Si la mascarilla colisiona con una cabeza
        if (collision.gameObject.tag == headTag) {
            Face face = collision.gameObject.GetComponent<Face>();
            // Le pone mascarilla al avatar
            if (!face.HasSurgicalMask()) {
                face.AddSurgicalMask();
            }
            // Destruye la mascarilla original
            Destroy(gameObject);
        }
    }
}
