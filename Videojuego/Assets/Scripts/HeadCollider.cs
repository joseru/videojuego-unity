﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UMA;
using UMA.CharacterSystem;

public class HeadCollider : MonoBehaviour
{
    public Vector3 center = new Vector3(-0.01750653f, 0.001595325f, 0.06732642f);
    public Vector3 size = new Vector3(0.3802873f, 0.2443287f, 0.2941619f);
    public string headTag = "head";
    GameObject head;
    BoxCollider headBoxCollider;
    Rigidbody headRigidBody;
    Face face;

    void Update()
    {
        if (!head) {
            try {
                head = GetHead();
                headBoxCollider = CreateBoxCollider(head, center, size);
                head.tag = headTag;
                headRigidBody = CreateRigidBody(head);
                AddSurgicalMaskModifier(head);
                face = head.GetComponent<Face>();
            } catch (UnityException e) {
                //Debug.Log("Error in " + gameObject.name + " when finding avatar's head");
            }
        }
    }

    int rootIndex = 0;
    int globalIndex = 0;
	int positionIndex = 0;
	int hipsIndex = 0;
	int lowerBackIndex = 0;
	int spineIndex = 1;
    int spine1Index = 0;
	int neckIndex = 0;
	int headIndex = 0;
	int headAdjustIndex = 9;

    // Busca la cabeza en el esqueleto del UMA
    GameObject GetHead() {
        return this.gameObject.transform.GetChild(rootIndex).GetChild(globalIndex).GetChild(positionIndex)
        .GetChild(hipsIndex).GetChild(lowerBackIndex).GetChild(spineIndex).GetChild(spine1Index)
        .GetChild(neckIndex).GetChild(headIndex).GetChild(headAdjustIndex).gameObject;
    }

    BoxCollider CreateBoxCollider(GameObject gameObject, Vector3 center, Vector3 size) {
        BoxCollider boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.center = center;
        boxCollider.size = size;
        return boxCollider;
    }

    Rigidbody CreateRigidBody(GameObject gameObject) {
        Rigidbody rigidBody = gameObject.AddComponent<Rigidbody>();
        rigidBody.useGravity = false;
        rigidBody.mass = 0;
        (gameObject.GetComponent<Rigidbody>()).constraints = RigidbodyConstraints.FreezeAll;
        return rigidBody;
    }

    void AddSurgicalMaskModifier(GameObject gameObject) {
        gameObject.AddComponent<Face>();
        (gameObject.GetComponent<Face>()).avatar = GetComponent<DynamicCharacterAvatar>();
    }

    public Face GetFace() {
        return face;
    }
}
