﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootObject : MonoBehaviour
{
    public MyInput myInput;

    public Transform camera;
    public GameObject prefabProjectile;
    public Vector3 projectilePositionCreation = new Vector3(0f, 0f, 1.5f);
    public Vector3 projectileRotation = new Vector3(-90f, 180f, 0);
    public Animator weaponAnimator;
    public float speed = 60f;
    public float despawnTime = 60f;


    // Start is called before the first frame update
    void Start()
    {
        if (weaponAnimator == null)
            weaponAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        // Disparar
        if (myInput.Shoot() && (weaponAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))) {

            weaponAnimator.SetBool("shooting", true);
            
            Invoke("Shoot", 0.17f);

        } else {
            weaponAnimator.SetBool("shooting", false);
        }

    }

    void Shoot() {
        Vector3 relativePosition = camera.right * projectilePositionCreation.x + camera.up * projectilePositionCreation.y + camera.forward * projectilePositionCreation.z;
        GameObject projectileClone = Instantiate(prefabProjectile, camera.position + relativePosition, camera.rotation * Quaternion.Euler(projectileRotation));
        Rigidbody rigidbody = projectileClone.GetComponent<Rigidbody>();
        rigidbody.velocity = camera.forward * speed;

        Destroy(projectileClone, despawnTime);
    }

    
}
