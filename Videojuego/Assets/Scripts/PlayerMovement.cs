﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;

    public MyInput myInput;

    public Animator characterAnimator;

    public float speed = 6f;
    public float gravity = -9.81f;
    public float jumpHeight = 1f;
    public float groundDistance = 0.2f;
    public LayerMask groundMask;

    Vector3 playerFeetPosition;

    Vector3 velocity;
    bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        // Movimientos del personaje principal
        MovePlayer();
        
        UpdateJump();
    }

    void MovePlayer() {
        float x = myInput.Horizontal();
        float z = myInput.Vertical();

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        // Actualizamos el animator
        characterAnimator.SetFloat("Forward", z);
    }

    void UpdateJump() {
        // Si estamos en el suelo reseteamos la velocidad en el eje y
        playerFeetPosition = transform.position - transform.up * controller.height / 2;
        isGrounded = Physics.CheckSphere(playerFeetPosition, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0) {
            velocity.y = -2f;
        }

        // Salto
        if (myInput.Jump() && isGrounded) {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
        
        // Gravedad
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
}
