﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantPosition : MonoBehaviour
{
    // Start is called before the first frame update

    Vector3 position;
    void Start()
    {
        position = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = position;
    }
}
