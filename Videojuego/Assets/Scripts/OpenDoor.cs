﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public MyInput playerInput;

    private Animator doorAnimator;

    public float playerDistance = 0.2f;

    public LayerMask playerMask;

    public Transform center;

    private bool opened = false;
    public GameObject openDoorText;

    // Start is called before the first frame update
    void Start()
    {
        doorAnimator = GetComponent<Animator>();
        if (center == null){
            center = transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!opened && Physics.CheckSphere(center.position, playerDistance, playerMask)) {
            // Si no está abierta y estámos cerca mostramos el texto de abrir puerta.
            openDoorText.SetActive(true);

            // Si se está pulsando la F abrimos la puerta
            if (playerInput.Action()) {
                doorAnimator.SetBool("open", true);
                opened = true;
                openDoorText.SetActive(false);
            }

        } else if (!opened && Physics.CheckSphere(center.position, playerDistance + 0.1f, playerMask)) {
            // Si no abrimos la puerta y nos alejamos dejamos de mostrar el texto.
            openDoorText.SetActive(false);
        }
    }
}
