﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public GameObject player;
    public Animator cameraAnimator;

    public void play(int nivel) {
        SceneManager.LoadScene(nivel);
    }

    public void mostrar(GameObject objeto) {
        objeto.SetActive(true);
    }

    public void ocultar(GameObject objeto) {
        objeto.SetActive(false);
    }

    public void salir() {
        Application.Quit();
    }

    public void setCursorLocked(bool locked) {
        if (locked) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        } else {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void enablePlayerActions(float time) {
        Invoke("enablePlayerActions", time);
    }

    public void enablePlayerActions() {
        enablePlayerActions(player);
    }

    public void disablePlayerActions() {
        disablePlayerActions(player);
    }

    private void enablePlayerActions(GameObject player) {
        player.GetComponent<PlayerMovement>().enabled = true;
        player.GetComponent<CameraLook>().enabled = true;
        player.GetComponent<ShootObject>().enabled = true;
    }

    private void disablePlayerActions(GameObject player) {
        player.GetComponent<PlayerMovement>().enabled = false;
        player.GetComponent<CameraLook>().enabled = false;
        player.GetComponent<ShootObject>().enabled = false;
    }




    public void disableCameraAnimator(float time) {
        Invoke("disableCameraAnimator",time);
    }

    private void disableCameraAnimator() {
        cameraAnimator.enabled = false;
    }

}
