﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UMA;
using UMA.CharacterSystem;

public class Face : MonoBehaviour
{
    public DynamicCharacterAvatar avatar;
    public string maleSurgcialMaskName = "MaleSurgicalMask_Recipe";
    public string femaleSurgcialMaskName = "FemaleSurgicalMask_Recipe";
    string slotName = "Face";

    public bool HasSurgicalMask() {
        return avatar.GetWardrobeItemName(slotName) != "";
    }

    public void AddSurgicalMask(string surgicalMaskName) {
        avatar.SetSlot(slotName, surgicalMaskName);
        avatar.BuildCharacter();
    }

    public void AddSurgicalMask() {
        string raceName = avatar.activeRace.name;
        if (raceName.StartsWith("HumanMale"))
            AddSurgicalMask(maleSurgcialMaskName);
        else if (raceName.StartsWith("HumanFemale"))
            AddSurgicalMask(femaleSurgcialMaskName);
    }

    public void RemoveSurgicalMask() {
        avatar.ClearSlot(slotName);
        avatar.BuildCharacter();
    }
}
