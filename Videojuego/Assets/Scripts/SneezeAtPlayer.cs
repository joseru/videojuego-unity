﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SneezeAtPlayer : MonoBehaviour
{
    public Menu menu;
    public GameObject player;
    public LayerMask playerMask;
    public float distance;
    private HeadCollider head;
    public static int losingScene = 0;

    public float loseTime = 5f;

    private AudioSource audio;

    public GameObject youLostText;
    public GameObject youLostMenu;

    // Start is called before the first frame update
    void Start()
    {
        head = GetComponent<HeadCollider>(); 
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Comprobamos la distancia a la altura del jugador
        Vector3 checkPosition = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);

        if (Physics.CheckSphere(checkPosition, distance, playerMask)) {
            LookAtPlayer();
            sneezeSound();
            
            if (head.GetFace() && !head.GetFace().HasSurgicalMask())
                LoseGame();
        }
    }

    private void LoseGame() {
        Invoke("showLosingText", 1);
        Invoke("LoadLosingMenu", loseTime);
    }

    private void showLosingText() {
        youLostText.SetActive(true);
    }

    private void LoadLosingMenu() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        menu.disablePlayerActions();
        youLostMenu.SetActive(true);
    }

    private void LookAtPlayer() {
        Vector3 targetPostition = new Vector3( player.transform.position.x, 
                                        this.transform.position.y, 
                                        player.transform.position.z ) ;

        transform.LookAt( targetPostition ) ;
    }

    private void sneezeSound() {
        if (!audio.isPlaying)
        {
            audio.Play();
        }
    }

}
