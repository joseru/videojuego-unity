﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLook : MonoBehaviour
{
    public MyInput myInput;
    public Transform camera;
    public bool lockCursor = true;
    public float mouseSensitivity = 100f;
    float xRotation = 0f;


    // Start is called before the first frame update
    void Start()
    {
        if (lockCursor) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Controles para mover la cámara con el ratón
        RotateCamera();
        RotatePlayer();
    }

    void RotateCamera() {
        float mouseY = myInput.VerticalCamera() * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        // Bloquea al mirar hacia arriba no mires hacia atrás
        xRotation = Mathf.Clamp(xRotation, -90f, 39f);

        // Rota la camara junto al movimiento del mouseY
        camera.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
    }

    void RotatePlayer() {
        float mouseX = myInput.HorizontalCamera() * mouseSensitivity * Time.deltaTime;
        // Rota al jugador junto al movimiento del mouseX
        transform.Rotate(Vector3.up * mouseX);
    }
}
