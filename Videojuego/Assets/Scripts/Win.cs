﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    public Menu menu;
    public LayerMask playerMask;
    public float distance = 3f;
    public Vector3 secondCenter = new Vector3 (0f, 2f, 15f);

    public GameObject youWonText;
    public GameObject youWonMenu;
    public float winTime = 5f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Victory()) {
            WinGame();
        }
    }


    private bool Victory() {
        return Physics.CheckSphere(transform.position, distance, playerMask)
            || Physics.CheckSphere(transform.position + secondCenter, distance, playerMask);
    }

    private void WinGame() {
        Invoke("ShowWinningText", 1);
        Invoke("LoadWinningMenu", winTime);
    }

    private void ShowWinningText() {
        youWonText.SetActive(true);
    }

    private void LoadWinningMenu() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        menu.disablePlayerActions();
        youWonMenu.SetActive(true);
    }
}
